import React from 'react';
import ReactDOM from 'react-dom';
//Bootstrap 4.0.0-beta.2 CSS
import 'bootstrap/dist/css/bootstrap.css';
//Main CSS
import './styles/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
