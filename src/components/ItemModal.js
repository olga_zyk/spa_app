import React, {Component} from 'react';
import {
  Button,
  CardImg,
  Col,
  Form,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row, CardSubtitle
} from 'reactstrap';
import coffeeImage from '../coffee_img.png'; //Using local image for example

class ItemModal extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);

    this.state = {
      name: '',
      price: '',
      isModalOpen: this.props.isModalOpen || false,
    };

    this.openModal = this.openModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addCoffeeItem = this.addCoffeeItem.bind(this);
    this.closeModal = this.closeModal.bind(this);

    console.log('ItemModal constructor(): ', this.state);

  }


  openModal() {
    this.setState({
      isModalOpen: !this.props.isModalOpen
    });
    console.log('openModal(): ', this.state);

  }

  handleChange(event) {   //get event object from the handleChange
    this.setState({
      [event.target.name]: event.target.value,  //[] is to use 'name' as a property name
    });
    console.log('Submitted data:', event.target.value);
  }

  addCoffeeItem(event, itemData) {
    itemData = {name: this.state.name, price: this.state.price};
    this.props.saveCallback(itemData);

    this.closeModal();

    event.preventDefault();
  }

  closeModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  render() {
    return (
      <Row>
        <Button color="primary" onClick={this.openModal}>Add</Button>
        <Modal isOpen={this.state.isModalOpen} onExit={this.closeModal}>
          <Row>
            <Col>
              <Button outline className="float-right" color="secondary" onClick={this.closeModal}>Close</Button>
              <ModalHeader toggle={this.toggle}>
                Add new coffee to our CoffeeMenu&trade;
              </ModalHeader>
            </Col>
          </Row>
          <ModalBody>
            <CardImg top width="100%" src={coffeeImage} alt="coffee_cup_img"/>
            <CardSubtitle style={{textAlign: "center", fontStyle: "italic"}}>
              Behind Every Successful Person There Is a Substantial Amount of <strong>COFFEE</strong>
            </CardSubtitle>
            <Form onSubmit={this.addCoffeeItem}>
              <FormGroup>
                <Row>
                  <Col>
                    {/*Name:*/} <Input type="text"
                                       id="coffee-name"
                                       name="name"
                                       value={this.state.name}
                                       onChange={this.handleChange}
                                       placeholder="Type a coffee name"/>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {/*Price:*/} <Input type="text"
                                        id="coffee-price"
                                        name="price"
                                        value={this.state.price}
                                        onChange={this.handleChange}
                                        placeholder="Type price here"/>
                  </Col>
                </Row>
                <ModalFooter>
                  <Button outline color="primary" type="submit">Save</Button>
                </ModalFooter>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </Row>
    );
  }
}

export default ItemModal;