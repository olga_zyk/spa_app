import React, { Component } from 'react';
import { Col, Card, CardImg, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';


class Item extends Component {
  constructor(props) {
    super(props);

    this.handleDeleteClick = this.handleDeleteClick.bind(this, this.key);
    console.log(this.props);
  }

  handleDeleteClick() {
    // var testObj = { id: 1, name: 'Cappuccino', price: 2.19, img: this.props.value.img };
    // console.log(testObj, this.props.value);
    
    this.props.onItemDelete(this.props.value);
  }


  render() {
    return(
      <Col xs={12} md={4} lg={3}>
        <Card>
          <CardBody>
            <CardTitle style={{textAlign: "center"}}>{this.props.value.name}</CardTitle>
            <CardImg top width="100%" src={this.props.value.img} alt="coffee_cup_img" />
            <CardSubtitle style={{textAlign: "right"}}>{this.props.value.price + ' $'}</CardSubtitle>
          </CardBody>
          <Button outline color="danger" size="sm" onClick={this.handleDeleteClick}>Delete</Button>
        </Card>
      </Col>
    );
  }
}

export default Item;