import React, { Component } from 'react';
import Item from './Item';
import { Row } from 'reactstrap';
// import coffeeImage from './coffee_img.png'; //Using local image for example

class ItemList extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      initialItemList: props.itemList // internal state of ItemList
    };
  }

  render() {
    const items = this.state.initialItemList.map(item => 
      <Item key={item.id} id={item.id} name={item.name} price={item.price} value={item} onItemDelete={this.props.onItemDelete} />
    );
      
    return(
      <Row>
        {items}
      </Row>
    );
  }
}

export default ItemList;