import React, { Component } from 'react';
import { Container} from 'reactstrap';
import ItemList from './components/ItemList';
import ItemModal from './components/ItemModal';
import coffeeImage from './coffee_img.png'; //Using local image for example
import './styles/App.css';



class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
      itemList: App.getItemList(),
    };

    this.showItemModal = this.showItemModal.bind(this);
    this.saveModal = this.saveModal.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  static getItemList() {
    const initialList = [
      { id: 1, name: 'Cappuccino', price: 2.19, img: coffeeImage },
      { id: 2, name: 'Latte', price: 1.99, img: coffeeImage },
      { id: 3, name: 'Americana', price: 1.19, img: coffeeImage },
      { id: 4, name: 'Irish Coffee', price: 3, img: coffeeImage }
    ];

    return initialList;
  }

  showItemModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
    console.log('showItemModal(): ', this.state);
    
  }

  saveModal(name) {
    console.log('saveModal():', this);
    console.log('data from Modal:', name);

    const newItemList = this.state.itemList;
    newItemList.push({ id: newItemList[newItemList.length - 1].id + Math.floor(Math.random()*100) + 1,
                        name: name.name,
                        price: name.price,
                        img: coffeeImage,
    });

    this.setState({
      itemList: newItemList,

    });

  }

  deleteItem(item) {
    const newItemList = this.state.itemList;
    const itemPosition = newItemList.indexOf(item);
    if (itemPosition > -1) {
      console.log('removing item at index #', itemPosition);

      newItemList.splice(itemPosition, 1);
      this.setState({
        itemList: newItemList
      });
    }

  }

  render() {
    return (
        <Container>
          <header className="App-header">
            <h1 className="App-title">Here is your coffee, Sir</h1>
          </header>
          <ItemModal saveCallback={this.saveModal} />
          <ItemList itemList={this.state.itemList} onItemDelete={this.deleteItem} />
        </Container>
    );
  }
}

export default App;
